/*
 * 组件注释
 * @Author: wuq-l
 * @Date: 2022-09-01 09:29:17
 * @LastEditors: machinist_wq
 * @LastEditTime: 2023-01-31 14:57:39
 * @Description: 用户信息
 * 人生无常！大肠包小肠......
 */

import { defineStore } from "pinia";
// @ts-ignore
import { getToken, setToken, setExpiresIn, removeToken } from "@/utils/auth";
interface IState {
  name?: string;
  avatar?: string;
  roles: any[];
  permissions: any[];
  userType: string;
  token: string;
}

export default defineStore({
  id: "user",
  state: (): IState => ({
    name: "",
    avatar: "",
    roles: [],
    permissions: [],
    userType: "",
    token: getToken(),
  }),
  getters: {},
  actions: {
    addRoles() {
      this.roles = [1, 2, 3];
    },
    Login() {
      this.roles = [1, 2, 3];
    },
    getInfo() {
      return new Promise((resolve, reject) => {
        resolve(true);
      })
    },
    // 退出系统
    LogOut() {
      return new Promise((resolve, reject) => {
        // logout(state.token)
        //   .then(() => {
        //     commit("SET_TOKEN", "");
        //     commit("SET_ROLES", []);
        //     commit("SET_PERMISSIONS", []);
        //     removeToken();
        //     resolve();
        //   })
        //   .catch(error => {
        //     reject(error);
        //   });
      });
    },
    // 前端 登出
    FedLogOut() {
      return new Promise(resolve => {
        this.SET_TOKEN("");
        this.SET_ROLES([]);
        this.SET_PERMISSIONS([]);
        removeToken();
        // TODO 如无特殊要求这里可直接回到登录页(使用window.open)
        resolve(false);
      });
    },

    /* 同步更新数据 */
    SET_TOKEN(token: string) {
      sessionStorage.setItem("currentBreadcrumbs", JSON.stringify([]));
      this.token = token;
    },
    SET_NAME(name: string) {
      this.name = name;
    },
    SET_AVATAR(avatar: string) {
      this.avatar = avatar;
    },
    SET_USER_TYPE(userType: string) {
      this.userType = userType;
    },
    SET_ROLES(roles: any[]) {
      this.roles = roles;
    },
    SET_PERMISSIONS(permissions: any[]) {
      this.permissions = permissions;
    },
  },
});
