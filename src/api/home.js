/*
 * 组件注释
 * @Author: dingcl-b
 * @Date: 2022-08-04 17:18:59
 * @LastEditors: machinist_wq
 * @LastEditTime: 2023-01-31 14:52:42
 * @Description:
 * 信誉管理接口
 */
import request from '@/utils/request'

// 系统消息查询
export const getSystem = (data) => {
  return request({
    url: '/v1/page/getSystemInfo',
    method: 'post',
    data: data
  })
}


//获取当前用户类型
export const getUserType = () => {
  return request({
    url: "/v1/page/getUserType",
    method: "get"
  })
}

