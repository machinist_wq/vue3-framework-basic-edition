/*
 * 组件注释
 * @Author: dingcl-b
 * @Date: 2022-07-04 14:25:19
 * @LastEditors: dingcl-b
 * @LastEditTime: 2022-07-19 10:26:13
 * @Description:
 * 人生无常！大肠包小肠......
 */
import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

const ExpiresInKey = 'Admin-Expires-In'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getExpiresIn() {
  return Cookies.get(ExpiresInKey) || -1
}

export function setExpiresIn(time) {
  return Cookies.set(ExpiresInKey, time)
}

export function removeExpiresIn() {
  return Cookies.remove(ExpiresInKey)
}
