/*
 * 组件注释
 * @Author: wuq-l
 * @Date: 2022-08-18 21:07:26
 * @LastEditors: machinist_wq
 * @LastEditTime: 2023-01-31 15:03:06
 * @Description: main 文件入口
 * 人生无常！大肠包小肠......
 */
import { createApp } from "vue";
import { createPinia } from "pinia";
import ElementPlus from "element-plus";
import locale from "element-plus/lib/locale/lang/zh-cn"; // 中文语言

import App from "./App.vue";
import router from "./router";
import directive from "./directive";
// 注册指令
import plugins from "./plugins"; // plugins
import { download } from '@/utils/request'
import "element-plus/dist/index.css";
import "./assets/main.css"; // 全局样式
import "./permission"; // permission control

const app = createApp(App);
// 全局方法挂载
app.config.globalProperties.download = download

// 初始化
app.use(createPinia());
app.use(router);
app.use(plugins);
app.use(directive);

// 使用element-plus 并且设置全局的大小
app.use(ElementPlus, {
  locale: locale,
  // 支持 large、default、small
  size: "default",
});
app.mount("#app");
