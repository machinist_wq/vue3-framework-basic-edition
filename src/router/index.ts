/*
 * 组件注释
 * @Author: machinist_wq
 * @Date: 2022-08-18 21:07:26
 * @LastEditors: machinist_wq
 * @LastEditTime: 2023-01-30 21:00:03
 * @Description:
 * 既往不恋！当下不杂！！未来不迎！！！
 */
import {
  createRouter,
  createWebHistory,
  createWebHashHistory,
} from "vue-router";
import HomeView from "@/views/HomeView.vue";

export const dynamicRoutes = [
  {
    path: "/system/user-auth",
    // component: Layout,
    hidden: true,
    permissions: ["system:user:edit"],
    children: [
      {
        path: "role/:userId(\\d+)",
        // component: () => import("@/views/system/user/authRole"),
        name: "AuthRole",
        meta: { title: "分配角色", activeMenu: "/system/user" },
      },
    ],
  },
];

export const constantRoutes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/AboutView.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  // history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: constantRoutes,
});
/* // 防止连续点击多次路由报错
let routerPush = router.prototype.push;
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(err => err);
}; */
export default router;
